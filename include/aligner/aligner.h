/**
 * @file aligner.h
 * @brief CLIPPER object-based alignment
 * @author Kaveh Fathian <kavehfathian@gmail.com>
 * @author Parker Lusk <plusk@mit.edu>
 * @date October 2021
 */

#pragma once

#include <memory>
#include <vector>
#include <string> 

#include <ros/ros.h>
#include <tf2_eigen/tf2_eigen.h>
#include <tf2_ros/transform_broadcaster.h>

#include <clipper/clipper.h>
#include <clipper/invariants/builtins.h>

#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point32.h>
#include <geometry_msgs/TransformStamped.h>
#include <nav_msgs/Path.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <visualization_msgs/Marker.h>
#include <visualization_msgs/MarkerArray.h>

// Eigen to Pose message & vice versa
#include <eigen_conversions/eigen_msg.h>
#include <std_msgs/Float64MultiArray.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/Transform.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Wrench.h>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/Dense>
#include <Eigen/StdVector>

EIGEN_DEFINE_STL_VECTOR_SPECIALIZATION(Eigen::Affine3d);

namespace clipperlcd {

  class Aligner
  {
  public:
    Aligner(const ros::NodeHandle& nh, const ros::NodeHandle& nhp);
    ~Aligner() = default;

  //---------- parameters --------------//
  private:
    static constexpr int DIM = 3; // assuming 3D points
    static constexpr int N = 3; // min number of objects required to estimate (R,t)
    
    double clipper_sigma = 3; // sigma for the Guassian weighting
    double clipper_epsilon = 6; // maximum error to consider two associations consistent
    double clipper_mindist = 2; // minimum distance between chosen objects in same map


  private:
    std::vector<std::string> robots;
    std::vector<std::string> trajectory_topics_;

    ros::NodeHandle nh_, nhp_;
    ros::Timer tim_aligner_; // time period for running clipper
    
    std::vector<ros::Subscriber> sub_r_landmarks_;
    std::vector<ros::Subscriber> sub_r_traj_; // robot trajectory
    
    std::vector<ros::Publisher> pubviz_r_map_;
    std::vector<ros::Publisher> pubviz_r_traj_;
    std::vector<ros::Publisher> pubviz_r_frame_;
    std::vector<ros::Publisher> pubviz_r_corres_;
    std::vector<ros::Publisher> pubviz_r_transform_;

    // tf broadcaster
    tf2_ros::TransformBroadcaster tfb_; // 

    int num_robots = 1; // number of robots
    double aligner_dt_; ///< Period of clipper runs in secods
    int num_match = 0; // objects matched by clipper
    std::string map_frame_id_;
    std::string world_frame_id_;

    std::vector<bool> r_first_data_; // has received first data
    // std::vector<std::vector<Eigen::Affine3d>> r_path_wrt_W_; // original paths

    
    std::vector<sensor_msgs::PointCloud2> r_landmarks_; // currently mapped landmarks of each robot
    std::vector<nav_msgs::Path> r_traj_; // robot trajectory

    // brief Problem Geometry
    std::vector<Eigen::Affine3d> T_WRi_; // robots initial position w.r.t world
    // std::vector<Eigen::Affine3d> T_WR_; // last received (i.e., current) robot poses
    std::vector<Eigen::Affine3d> T_R1iRii_; // robots' start w.r.t robot1 start : The Thing we want to estimate

    // CLIPPER
    clipper::Association Ain_;
    std::unique_ptr<clipper::CLIPPER<clipper::invariants::EuclideanDistance>> clipper_;    
    
    void align_maps(const uint i);

    // ROS callbacks
    void timer_cb(const ros::TimerEvent& event);
    void landmarks_cb(const sensor_msgs::PointCloud2ConstPtr& msg,  uint i);
    void traj_cb(const nav_msgs::PathConstPtr& msg,  uint i);

    // functions
    void transform_traj(const Eigen::Affine3d& T, 
                        const nav_msgs::Path& traj, 
                        nav_msgs::Path& traj_new); // transofrm trajecotry basaed on affine trasform T
    
  };

} // ns lcd
